Jamielynn is a newborn photographer in Medford, New Jersey. She specializes in child, newborn, maternity and family photography in South Jersey. Serving Philadelphia, South Jersey, Burlington, Camden, Gloucester and Ocean Counties for all of your portrait photography needs.

Website : http://www.lifecapturedbyjamielynn.com